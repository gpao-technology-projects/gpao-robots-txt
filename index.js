import fetch from 'node-fetch';
import fs, { appendFile } from 'fs'
import path from 'path'
import 'dotenv/config' 
import express from  'express';
import simpleGit, { CleanOptions } from 'simple-git';
import { request } from 'http';
//import nodegit from 'nodegit'

const __dirname = path.resolve();
console.log(__dirname)

let port = process.env.PORT;
let auth = process.env.AUTH;
let bit_user = process.env.BIT_USER;
let bit_pass = process.env.BIT_PASS;

simpleGit().clean(CleanOptions.FORCE);
const git = simpleGit();

var remoteBranchName = "https://ben-yixin@bitbucket.org/gpao-technology-projects/gpao-robots-txt.git";
/*
var getMostRecentCommit = function(repository) {
  return repository.getBranchCommit("production");
};

var getCommitMessage = function(commit) {
  return commit.message();
};

nodegit.Repository.open(__dirname)
  .then(getMostRecentCommit)
  .then(getCommitMessage)
  .then(function(message) {
    console.log(message);
  });
/*
nodegit.Remote.create(__dirname, "test", remoteBranchName).then((remote) => {
  console.log(remote)
})*/


const app = express();

getConfluence();
var interval = setInterval(getConfluence, 60000);

app.get("/", (req,res) => {
  res.json({Status: 'Running'});
})
app.get("/file", (req,res) => {
  res.sendFile(path.join(__dirname+'/index.html'));
})

app.get("/confluence", (req,res) => {
  request.get()
})

function getConfluence()
{
  fetch(
    "https://gpnzit.atlassian.net/wiki/rest/api/content/1464074248?expand=body.storage",
    {
      method: "GET",
      headers: {
        Authorization:
          `Basic ${auth}`,
        Accept: "application/json"
      }
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.json();
    })
    .then((text) => {
      //console.log("Body:", text.body.storage.value);
      var tagRegex = /<a [^>]*>/gi;
      var urlRegex = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/;
      var links = text.body.storage.value.match(tagRegex);
  
      var donateURL = [];
      var petitionURL = [];
      var cloudURL = [];
      var subscribeURL = [];
      var noCategory = [];
      for (var link of links) {
        //TODO switch case

        //console.log(`Sorting ${link}`);
        if (link.includes("donate.act")) {
          //console.log("Sorted to Donate: " + link.match(urlRegex)[2]);
          donateURL.push(link.match(urlRegex)[2]);
        } else if (link.includes("petition.act")) {
          // console.log(link.match(urlRegex)[2]);
          petitionURL.push(link.match(urlRegex)[2]);
        } else if (link.includes("cloud.act")) {
          // console.log(link.match(urlRegex)[2]);
          cloudURL.push(link.match(urlRegex)[2]);
        } else if (link.includes("subscribe.act")) {
          // console.log(link.match(urlRegex)[2]);
          subscribeURL.push(link.match(urlRegex)[2]);
        } else {
          //console.log("No category found for: " + link.match(urlRegex)[2]);
          noCategory.push(link.match(urlRegex)[0]);
        }
      }
  
      const bots = ["Googlebot", "Bingbot", "Slurp", "DuckDuckBot", "facebot"];
      var content = "";
      for (var bot of bots) {
        content += "\nUser-agent: " + bot;
        for (var URL of petitionURL) {
          content += "\nAllow: " + URL;
        }
        content += "\nDisallow: /\n";
      }
      content += "\nUser-agent: * \nDisallow: /";
  
      fs.writeFile(`${__dirname}/robots/petition-robots.txt`, content, (err) => {
        if (err) {
          console.error(err);
          return;
        }

      });
      fs.watch(`${__dirname}/robots/petition-robots.txt`, (eventType, filename) =>{
        console.log(`${filename} was modified`)
        console.log(`${eventType} was the event`)
        //file written successfully
        console.log("File write success");
        try {
          git
          .init()
          .addRemote('origin'
          , `https://${bit_user}@bitbucket.org/gpao-technology-projects/gpao-robots-txt.git`)
          .fetch();

          simpleGit().addConfig('user.email', 'bzhang@greenpeace.org');
          simpleGit().addConfig('user.name', 'ben-yixin');
/*
          git.pull('origin','production')
          .then(()=>{console.log("Pulled!")})
          .catch((err) => {console.error("Pulling error: ",err)});
 */
/*
          git.clone(`https://${bit_user}@bitbucket.org/ben-yixin/test.git`)
          .then(()=>{console.log("Cloned!")})
          .catch((err) => {console.error("Cloning error: ",err)});
*/
/*
          simpleGit().commit("simplegitautocommit", `${__dirname}/robots/petition-robots.txt`)
          .then(() => console.log("commit"))
          .catch((err) => console.error("commit failed ", err));

          simpleGit().push("origin", "production")
          .then(() => console.log("push"))
          .catch((err) => console.error("push failed ", err));
         
          /*
          simpleGit().add(`${__dirname}/test.txt`)
          .then(() => console.log("added file"))
          .catch((err) => {
            console.log(err)
          })

          simpleGit().commit("testfile", `${__dirname}/test.txt`)
          .then(() => console.log("test committed"))
          .catch((err) => {
            console.log(err)
          })
*/
        //simpleGit().push("heroku", "production:main")
      } catch(err){
        console.error(err);
      }
        
      })
      console.log("No Category: ", noCategory);
      console.log("Donate: ", donateURL);
      console.log("Petition: ", petitionURL);
      console.log("Cloud: ", cloudURL);
      console.log("Sub: ", subscribeURL);

    })
    .catch((err) => console.error(err));  
}
app.listen(port)


  
